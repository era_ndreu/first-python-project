# Write a function to check if a number is even.
def even(number):
    if number % 2 == 0:
        return True
    else:
        return False


result = even(2)
print(result)
result = even(3)
print(result)


# Write a function to check if a number is palindrome.
def palindrome(number):
    temp = number
    reverse = 0
    while number > 0:
        remainder = number % 10
        reverse = reverse * 10 + remainder
        number = number // 10

    if temp == reverse:
        return True
    else:
        return False


result = palindrome(1221)
print(result)


# Write a function to check if a number is power of 2.
def power2(number):
    if number == 0:
        return False
    while number != 1:
        if number % 2 != 0:
            return False
        number = number // 2
    return True


result = power2(4)
print(result)


# Write a function to convert a given string to all uppercase if it contains at least
# 2uppercase characters in the first 4 characters.
def upper(string):
    count = 0
    str1 = string[0:3]
    for i in str1:
        if i.isupper():
            count = count + 1
    if count >= 2:
        string = string.upper()
    return string


result = upper("ERRa")
print(result)


# Given a list iterate it and display numbers which are divisible by 5 and if you
# find number greater than 150 stop the loop iteration
list1 = [12, 15, 32, 42, 55, 75, 122, 132, 150, 180, 200]
for i in list1:
    if i % 5 == 0:
        if i > 150:
            break
        else:
            print(i)


# Write a function that takes a list of words and groups the words by their length
# using a dictionary
def my_dictionary(list1):
    dict1 = {}
    for i in list1:
        count = len(i)
        if dict1.get(count) is None:
            dict1[count] = i
        else:
            if not isinstance(dict1[count], list):
                dict1[count] = [dict1[count]]
            dict1[count].append(i)

    return sorted(dict1.items(), key=lambda t: t[0])


my_list = ['Lorem', 'ele', 'sit', 'incididunt', 'a', 'su', 'dom', 'ouch', 'ipsum']
result = my_dictionary(my_list)
print(result)


# Create a class Person with first_name and last_name as constructor
# parameters. The class should have a function get_fullname which returns the
# person’s full name (first_name + last_name). Create another class Student,
# inheriting from Person. Student’s get_fullname function should return students
# full name followed by ‘-st.’
class Person:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

    def get_fullname(self):
        return self.first_name + " " + self.last_name


p1 = Person("Era", "Ndreu")
print(p1.get_fullname())


class Student(Person):
    def __init__(self, first_name, last_name):
        super().__init__(first_name, last_name)

    def get_fullname(self):
        return self.first_name + " " + self.last_name + " " + "-st."


p2 = Student("Elsa", "Ndreu")
print(p2.get_fullname())
